$(function(){

  // Deal Model
  // ----------

  window.Deal = Backbone.Model.extend({

  });

  // Deal Collection
  // ---------------

  window.DealList = Backbone.Collection.extend({

    // Reference to this collection's model.
    model: Deal,
    url: '/deals.json?latitude=12.9103951&longitude=77.59139429999999'

  });

  // Create our global collection of **Deals**.
  window.Deals = new DealList;

  // Deal Item View
  // --------------

  // The DOM element for a Deal item...
  window.DealView = Backbone.View.extend({

    //... is a list tag.
    tagName:  "div",

    // Cache the template function for a single item.
    template: _.template($('#deal-template').html()),

    // Re-render the contents of the todo item.
    render: function() {
      $(this.el).html(this.template(this.model.toJSON()));
      this.setText();
      return this;
    },

    // To avoid XSS (not that it would be harmful in this particular app),
    // we use `jQuery.text` to set the contents of the todo item.
    setText: function() {
      var bizName = this.model.get('business_name');
      this.$('#deal-name').text(this.model.get('name'));
      this.$('#business-name').text(this.model.get('business_name'));
      var address = this.model.get('address');
      var latitude = this.model.get('latitude');
      var longitude = this.model.get('longitude');
      var info = "<b>"+bizName+"</b><br/>"+address;
      this.$('#show-details').click(function(){showMap(latitude,longitude,info)});
    }
  });

  // The Application
  // ---------------

  // Our overall **AppView** is the top-level piece of UI.
  window.AppView = Backbone.View.extend({

    // Instead of generating a new element, bind to the existing skeleton of
    // the App already present in the HTML.
    el: $("#app"),

    // At initialization we bind to the relevant events on the `Deals`
    // collection, when items are added or changed. Kick things off by
    // loading any preexisting todos that might be saved in *localStorage*.
    initialize: function() {
      Deals.bind('reset', this.addAll, this);
      Deals.fetch();
    },

    // Re-rendering the App just means refreshing the statistics -- the rest
    // of the app doesn't change.
    render: function() {
    },

    // Add a single todo item to the list by creating a view for it, and
    // appending its element to the `<ul>`.
    addOne: function(deal) {
      var view = new DealView({model: deal});
      $("#deal-list").append(view.render().el);
    },

    // Add all items in the **Deals** collection at once.
    addAll: function() {
      Deals.each(this.addOne);
    },
  });
  // Finally, we kick things off by creating the **App**.
  window.App = new AppView;

});

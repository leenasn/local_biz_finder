require({
  baseUrl: 'scripts',
  paths: {
    jquery: '/scripts/jquery.min',
  },
  priority: ['jquery']
});

require(["jquery", "/scripts/underscore.js","/scripts/backbone.js","/scripts/deal.js"], function($) {
    $(function() {
    });
});

function showMap(lat,lng) {
  $("em.default-msg, #map-with-address").hide();
  $("#map-without-address").show();
  $("#map-without-address").click(function(){
    showMapWithAddress();
  });
}

function showMapWithAddress() {
  $("#map-without-address, em.default-msg").hide();
  $("#map-with-address").show();
}


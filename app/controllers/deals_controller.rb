class DealsController < ApplicationController
  def index
    latitude = params[:latitude]
    longitude = params[:longitude]
    if latitude.blank? || longitude.blank?
      render :json => {:error => "Missing parameters"},:status => 400
    else
      deals = Deal.all.to_json(:methods => [:address,:business_name,:latitude,:longitude])
      render :json => deals
    end
  end
end

class BusinessesController < ApplicationController
  respond_to :json

  # GET /businesses.json
  def index
    latitude = params[:latitude]
    longitude = params[:longitude]
    if latitude.blank? || longitude.blank?
      render :json => {:error => "Missing parameters"},:status => 400
    else
      render :json => Business.near([latitude,longitude])
    end
  end

  # GET /businesses/1.json
  def show
    render :json => Business.find(params[:id])
  end
end

class Deal < ActiveRecord::Base
  belongs_to :business
  delegate :address,:latitude,:longitude, :to => :business
  delegate :name, :to => :business,:prefix => :business
end

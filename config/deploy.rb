require "bundler/capistrano"

set :application, "ilocate"
set :repository, "ssh://git@bitbucket.org/leenasn/local_biz_finder.git"
set :branch,"master"
set :use_sudo, false
set :scm, "git"

namespace :deploy do
  namespace :db do
    desc "After deploy:setup, create a shared database.yml"
    task :setup, :roles => :app do
      template = <<-EOF
base: &base
  adapter: sqlite3
  timeout: 5000
#{rails_env}:
    database: "#{shared_path}/config/#{db_name}"
    <<: *base
        EOF
      config = ERB.new(template)
      run "mkdir -p #{shared_path}/config"
      put config.result(binding), "#{shared_path}/config/database.yml"
    end

    desc <<-DESC
        [internal] Updates the symlink for database.yml file to the just deployed release.
      DESC
    task :symlink, :except => { :no_release => true } do
      run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    end
    after "deploy:setup", "deploy:db:setup"
    after "deploy:update_code", "deploy:db:symlink"
  end

  task :start, :roles => :app do
    run "cd #{deploy_to}/current; bundle exec thin -C thin_#{rails_env}.yml start"
  end
  task :stop, :roles => :app do
    run "cd #{deploy_to}/current;bundle exec  thin -C thin_#{rails_env}.yml stop"
  end
  task :restart, :roles => :app do
    run "cd #{deploy_to}/current;bundle exec thin -C thin_#{rails_env}.yml restart"
  end
end




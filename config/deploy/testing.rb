set :deploy_to,"/Users/vaidy/ilocate"
set :user,"vaidy"
set:password,"password"
set :db_name, "local_biz_finder_test"

role :web,      "test.ilocate.com"
role :app,      "test.ilocate.com"
role :db,       "test.ilocate.com", :primary => true

set :default_environment, {
  'PATH' => "/Users/vaidy/.rvm/rubies/ruby-1.9.3-p0/bin:/Users/vaidy/.rvm/gems/ruby-1.9.3-p0@global/bin:/home/elixir/.rvm/gems/ruby-1.9.3-p0/bin:/Users/vaidy/.rvm/bin:$PATH",
  'RUBY_VERSION' => 'ruby 1.9.2',
  'GEM_HOME'     => '/Users/vaidy/.rvm/gems/ruby-1.9.3-p0',
  'GEM_PATH'     => '/Users/vaidy/.rvm/gems/ruby-1.9.3-p0@global',
}


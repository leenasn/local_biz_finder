require "spec_helper"

describe DealsController do
  describe "routing" do
    describe "valid routes" do
      it "#index" do
        get("/deals").should route_to("deals#index")
      end
    end

    describe "invalid routes" do
      it "#create" do
        post("businesses").should_not be_routable
      end

      {
        "deals/new" => "get",
        "create" => "post",
        "edit/1" => "get",
        "update/1" => "put",
        "deals/1" => "get",
        "destroy/1" => "delete" }.each do |action, method|
        it "##{action}" do
          send(method, "businesses/#{action.dup}").should_not be_routable
        end
      end
    end
  end
end

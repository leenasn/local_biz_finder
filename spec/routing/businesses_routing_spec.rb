require "spec_helper"

describe BusinessesController do
  describe "routing" do
    describe "valid routes" do
      it "#index" do
        get("/businesses").should route_to("businesses#index")
      end
      it "#show" do
        get("/businesses/1").should route_to("businesses#show", :id => "1")
        get("/businesses/new").should route_to("businesses#show", :id => "new")
      end
    end

    describe "invalid routes" do
      it "#create" do
        post("businesses").should_not be_routable
      end

      { "edit/1" => "get",
      "update/1" => "put",
      "destroy/1" => "delete" }.each do |action, method|
        it "##{action}" do
          send(method, "businesses/#{action.dup}").should_not be_routable
        end
      end
    end
  end
end

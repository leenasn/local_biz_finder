require 'spec_helper'
require 'capybara/rails'
require 'capybara/rspec'

DatabaseCleaner.strategy = :truncation

RSpec.configure do |config|
  #Capybara.register_driver :selenium do |app|
  #  Capybara::Selenium::Driver.new(app, :browser => :chrome)
  #end
  config.use_transactional_fixtures = false
  config.before :each do
    DatabaseCleaner.start
  end
  config.after :each do
    DatabaseCleaner.clean
  end
end

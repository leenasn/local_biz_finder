# Read about factories at http://github.com/thoughtbot/factory_girl


Factory.define :away_business , :class => Business do |business|
  business.name "Edge Fitness"
  business.address "46th Cross Road, JP Nagar, Bangalore, Karnataka, 560011"
  business.description "Fitness centre"
end

Factory.define :local_business, :class => Business do | business |
  business.name "Azkinetic Fitness"
  business.address "JP Nagar Phase 2, Near Police Station, Bangalore, Karnataka, 560078"
  business.description "Fitness centre"
end



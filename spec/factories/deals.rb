# Read about factories at http://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :deal1, :class => Deal do
    name "60% off"
    start_date Time.now
    end_date Time.now + 10.days
  end
end

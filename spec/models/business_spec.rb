require 'spec_helper'

describe Business do
  use_vcr_cassette
  let(:local_business){Factory(:local_business)}
  let(:away_business){Factory(:away_business)}
  let(:coordinates){[12.9103951,77.59139429999999]}

  it "populate the lat/long from address" do
    local_business.latitude.should_not be_nil
    local_business.longitude.should_not be_nil
  end

  it "find businesses within 5 miles for a given address" do
    Business.near(coordinates, 0.5).should == [local_business]
  end

  it "find businesses within 10 miles for a given address" do
    Business.near(coordinates, 1).should == [local_business,away_business]
  end
end

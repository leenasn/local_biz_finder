require 'spec_helper'

describe Deal do
  use_vcr_cassette
  let(:business){Factory(:local_business)}
  let(:deal){Factory(:deal1,:business => business)}

  it "address should be same as business address" do
    deal.address.should eq business.address
  end
  it "business name" do
    deal.business_name.should eq business.name
  end
  it "latitude, longitude" do
    deal.latitude.should eq business.latitude
    deal.longitude.should eq business.longitude
  end
end

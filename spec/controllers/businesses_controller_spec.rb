require 'spec_helper'

describe BusinessesController do
  use_vcr_cassette
  let!(:local_business){Factory(:local_business)}
  let!(:away_business){Factory(:away_business)}
  let(:latitude){12.9103951}
  let(:longitude){77.59139429999999}

  describe "List" do
    it "list nearby businesses within 1 mile" do
      Business.stub(:near).and_return([local_business,away_business])
      get :index,{:latitude => latitude,:longitude => longitude,:format => :json}
      response.body.should eql([local_business,away_business].to_json)
      response.status.should eql 200
    end

    it "returns invalid response if both latitude and longitude parameters are missing" do
      get :index
      response.status.should eql 400
    end
    it "returns invalid response if the latitude parameter is missing" do
      get :index, {:longitude => longitude,:format => :json}
      response.status.should eql 400
    end
    it "returns invalid response if longitude parameter is missing" do
      get :index, {:latitude => latitude,:format => :json}
      response.status.should eql 400
    end
  end
  describe "Show Details" do
    it "Display the details for a given business" do
      Business.stub(:find).and_return(local_business)
      get :show,{:id => local_business.id,:format => :json}
      response.body.should == local_business.to_json
    end
  end
end

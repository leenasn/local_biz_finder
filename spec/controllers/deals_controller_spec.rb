require 'spec_helper'

describe DealsController do
  use_vcr_cassette
  let(:local_business){Factory(:local_business)}
  let(:away_business){Factory(:away_business)}
  let!(:deal1){Factory(:deal1,:business => local_business)}
  let(:deal2){Factory(:deal1,:business => away_business)}
  let(:latitude){12.9103951}
  let(:longitude){77.59139429999999}

  it "list nearby deals" do
    local_business.stub(:deals).and_return(deal1)
    Deal.stub(:all).and_return(deal1)
    get :index,{:latitude => latitude,:longitude => longitude,:format => :json}
    response.body.should eql(deal1.to_json(:methods => [:address,:business_name,:latitude,:longitude]))
    response.status.should eql 200
  end
end

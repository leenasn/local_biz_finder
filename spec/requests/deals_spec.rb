require 'integration_spec_helper'

describe 'Deals', :js => true do
  use_vcr_cassette
  let!(:business){Factory(:local_business)}
  let!(:deal){Factory(:deal1,:business => business)}

  before(:each) do
    visit('/pages/widgets')
  end

  describe "#list" do
    it 'list the running deals' do
      visit('/pages/widgets')
      find(:id,'deal-name').should have_content(deal.name)
      find(:id,'business-name').should have_content(business.name)
    end
  end

end
